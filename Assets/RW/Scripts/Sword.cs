﻿using UnityEngine;


public class Sword : MonoBehaviour
{
    [SerializeField]
    private GameEvent onSwordSelect; 

    private void OnMouseDown()
    {
        onSwordSelect.Raised();
    }
}
