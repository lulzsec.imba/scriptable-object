using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SwordData", menuName = "SwordData", order = 51)]
public class SwordData : ScriptableObject
{
    [SerializeField] private string swordName;
    [SerializeField] private Sprite icon;
    [SerializeField] private int goldCost;
    [SerializeField] private string description;
    [SerializeField] private int attackDamage;

    public string SwordName
    {
        get
        {
            return swordName;
        }
    }

    public string Description
    {
        get
        {
            return description;
        }
    }

    public Sprite Icon
    {
        get
        {
            return icon;
        }
    }

    public int GoldCost
    {
        get
        {
            return goldCost;
        }
    }

    public int AttackDamage
    {
        get
        {
            return attackDamage;
        }
    }
}
