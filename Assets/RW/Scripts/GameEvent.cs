using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGameEvent", menuName = "GameEvent", order = 52)]
public class GameEvent : ScriptableObject
{
    private List<GameEventListener> gameEventListeners = new List<GameEventListener>();

    public void Raised()
    {
        for(int i = gameEventListeners.Count - 1; i >= 0; i--)
        {
            gameEventListeners[i].OnRaisedEvent();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        gameEventListeners.Add(listener);
    }

    public void UnRegisterListener(GameEventListener listener)
    {
        gameEventListeners.Remove(listener);
    }
}
